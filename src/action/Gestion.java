/**
 * 
 */
package action;

import java.io.File;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import util.ScannerUtil;
import validator.CommandeValidator;

/**
 * Classe qui gère la commande mybrowsersync et la validation de son format
 * 
 * @author Charline
 *
 */
public class Gestion {

    /**
     * Constructeur par defaut
     */
    public Gestion() {
        super();
    }

    /**
     * permet de gerer les commandes lues
     * 
     * @param file de l'emplacement en cours
     * @param commande lue
     */
    public static void gestionCommande(final File file, final String commande) {

        File path = new File(file.getPath());
        final Commande methodesCmd = new Commande();
        String[] cmdTab = extractionCommande(commande);
        String cmd = cmdTab[0];
        String saisie = "";
        while (cmd != "exit") {

            if (gestionValidator(cmd.toLowerCase())) {

                String source = cmdTab[1];

                switch ((cmd.toLowerCase())) {
                    case "mybrowsersync" :
                        Commande.mybrowsersync(path, source);
                        break;

                    case "exit" :
                        System.exit(0);
                        break;

                    default :
                        System.out.print(path.getAbsolutePath() + "> commande inconnu : tapper 'mybrowsersync' ");
                        break;
                }
            } else {
                System.out.println(path.getAbsolutePath() + "> commande inconnu : tapper 'mybrowsersync' ");
            }
            path = new File(methodesCmd.getFile().getAbsolutePath());
            System.out.print(path.getAbsolutePath() + ">");
            saisie = ScannerUtil.lire();
            cmdTab = extractionCommande(saisie);
            cmd = cmdTab[0];
        }
        System.exit(0);
    }

    /**
     * Permet de gerer les validators de commande
     * 
     * @param commande lue
     * @return true si le format est correct sinon false
     */
    public static boolean gestionValidator(final String commande) {
        Boolean valide = false;
        switch (commande) {
            case "mybrowsersync" :
                valide = CommandeValidator.isValideMybrowsersync(commande);
                break;
            case "exit" :
                valide = true;
                break;
            default :
                valide = false;
                break;
        }

        return valide;
    }

    /**
     * Permet de décomposer la commande lue
     * 
     * @param commande lue
     * @return tab tableau contenant les composants de la commande
     */
    public static String[] extractionCommande(final String commande) {
        final String[] tab = new String[20];
        int i = 0;
        final Pattern pattern = Pattern.compile("[a-zA-Z0-9_\\/.-]+", Pattern.CASE_INSENSITIVE);
        final Matcher matches = pattern.matcher(commande);

        while (matches.find()) {
            tab[i] = matches.group();
            i++;
        }
        return tab;
    }

}
