package action;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Date;
import java.util.StringTokenizer;

public class ServeurHTTPJava implements Runnable {

    static final File    WEB_ROOT             = new File(".");
    static final String  DEFAULT_FILE         = "src\\vue\\index.html";
    static final String  FILE_NOT_FOUND       = "src\\vue\\404.html";
    static final String  FORMAT_NOT_SUPPORTED = "src\\vue\\not_supported.html";
    static final int     PORT                 = 8080;

    // verbose mode
    static final boolean verbose              = true;
    private Socket       socket;

    public ServeurHTTPJava(Socket s) {
        socket = s;
    }

    @Override
    public void run() {

        BufferedReader bufferedReader = null;
        PrintWriter printWriter = null;
        BufferedOutputStream bufferedOutputStream = null;
        String fileRequested = null;

        try {
            bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            printWriter = new PrintWriter(socket.getOutputStream());
            bufferedOutputStream = new BufferedOutputStream(socket.getOutputStream());

            final String input = bufferedReader.readLine();
            final StringTokenizer parse = new StringTokenizer(input);
            final String method = parse.nextToken().toUpperCase();

            fileRequested = parse.nextToken().toLowerCase();

            if (!method.equals("GET") && !method.equals("HEAD")) {
                if (verbose) {
                    System.out.println("501 Not Implemented : " + method + " method.");
                }

                final File file = new File(WEB_ROOT, FORMAT_NOT_SUPPORTED);
                final int fileLength = (int) file.length();
                final String contentMimeType = "text/html";
                final byte[] fileData = lireFile(file, fileLength);

                printWriter.println("HTTP/1.1 501 Not Implemented");
                printWriter.println("Server: Java HTTP Server from SSaurel : 1.0");
                printWriter.println("Date: " + new Date());
                printWriter.println("Content-type: " + contentMimeType);
                printWriter.println("Content-length: " + fileLength);
                printWriter.println();
                printWriter.flush();

                bufferedOutputStream.write(fileData, 0, fileLength);
                bufferedOutputStream.flush();

            } else {
                if (fileRequested.endsWith("/")) {
                    fileRequested += DEFAULT_FILE;
                }

                final File file = new File(WEB_ROOT, fileRequested);
                final int fileLength = (int) file.length();
                final String content = getContentType(fileRequested);

                if (method.equals("GET")) {
                    final byte[] fileData = lireFile(file, fileLength);

                    printWriter.println("HTTP/1.1 200 OK");
                    printWriter.println("Server: Java HTTP Server from SSaurel : 1.0");
                    printWriter.println("Date: " + new Date());
                    printWriter.println("Content-type: " + content);
                    printWriter.println("Content-length: " + fileLength);
                    printWriter.println();
                    printWriter.flush();

                    bufferedOutputStream.write(fileData, 0, fileLength);
                    bufferedOutputStream.flush();
                }

                if (verbose) {
                    System.out.println("File " + fileRequested + " of type " + content + " returned");
                }

            }

        } catch (final FileNotFoundException fileNotFoundException) {
            try {
                fileNotFound(printWriter, bufferedOutputStream, fileRequested);
            } catch (final IOException ioe) {
                System.err.println("Error with file not found exception : " + ioe.getMessage());
            }

        } catch (final IOException ioe) {
            System.err.println("Server error : " + ioe);
        } finally {
            try {
                bufferedReader.close();
                printWriter.close();
                bufferedOutputStream.close();
                socket.close();
            } catch (final Exception e) {
                System.err.println("Error closing stream : " + e.getMessage());
            }

            if (verbose) {
                System.out.println("Connection fermée !\n");
            }
        }

    }

    /**
     * Permet de lire le contenu d'un fichier et le stock dans un tableau
     * 
     * @param file de l'emplacement du fichier à lire
     * @param fileLength taille du fichier à lire
     * @return tableau contenant les informations du fichier à lire
     * @throws IOException exception levée si erreur de lecture
     */
    private byte[] lireFile(final File file, final int fileLength) throws IOException {
        FileInputStream fileIn = null;
        final byte[] fileData = new byte[fileLength];

        try {
            fileIn = new FileInputStream(file);
            fileIn.read(fileData);
        } finally {
            if (fileIn != null)
                fileIn.close();
        }

        return fileData;
    }

    /**
     * Recupère l'extension du fichier 
     * @param fileRequested du fichier 
     * @return le type MIME du fichier
     */
    private String getContentType(final String fileRequested) {
       
        if (fileRequested.endsWith(".htm")||fileRequested.endsWith(".html")) {
            return "text/html";
        }
        if (fileRequested.endsWith(".csv")) {
            return "text/csv";
        }
        if (fileRequested.endsWith(".css")) {
            return "text/css";
        }

        if (fileRequested.endsWith(".jpeg")) {
            return "image/jpeg";
        }
        if (fileRequested.endsWith(".png")) {
            return "image/jpeg";
        }
        if (fileRequested.endsWith(".gif")) {
            return "image/gif";
        }
        if (fileRequested.endsWith(".svg")) {
            return "image/svg+xml";
        }
        
        if (fileRequested.endsWith(".tiff")||fileRequested.endsWith(".tif")) {
            return "image/tiff";
        }
        if (fileRequested.endsWith(".xhtml")) {
            return "application/xhtml+xml";
        }
        if (fileRequested.endsWith(".xml")) {
            return "application/xml";
        }
        if (fileRequested.endsWith(".pdf")) {
            return "application/pdf";
        }
        if (fileRequested.endsWith(".jar")) {
            return "application/java-archive";
        }
        if (fileRequested.endsWith(".js")) {
            return "application/javascript";
        }
        if (fileRequested.endsWith(".json")) {
            return "application/json";
        }
        if (fileRequested.endsWith(".tar")) {
            return "application/x-tar";
        }
        if (fileRequested.endsWith(".wav")) {
            return "application/wav";
        }
        if (fileRequested.endsWith(".ttf")) {
            return "font/ttf";
        }
        return "text/plain";
    }

    /**
     * Gère le message de l'exception FileNotFound
     * 
     * @param printWriter des données
     * @param outputStream du flux en cours
     * @param fileRequested du fichier
     * @throws IOException exception levée
     */
    private void fileNotFound(final PrintWriter printWriter, final OutputStream outputStream, final String fileRequested) throws IOException {
        final File file = new File(WEB_ROOT, FILE_NOT_FOUND);
        final int fileLength = (int) file.length();
        final String content = "text/html";
        final byte[] fileData = lireFile(file, fileLength);

        printWriter.println("HTTP/1.1 404 File Not Found");
        printWriter.println("Server: Java HTTP Server from SSaurel : 1.0");
        printWriter.println("Date: " + new Date());
        printWriter.println("Content-type: " + content);
        printWriter.println("Content-length: " + fileLength);
        printWriter.println();
        printWriter.flush();

        outputStream.write(fileData, 0, fileLength);
        outputStream.flush();

        if (verbose) {
            System.out.println("File " + fileRequested + " not found");
        }
    }

}
