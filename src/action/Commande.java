/**
 * 
 */
package action;

import java.io.File;
import java.io.IOException;
import java.net.ServerSocket;

/**
 * Classe qui implemente les commandes
 * 
 * @author 59013-07-04
 *
 */
public class Commande {

    private File                file = new File("");
    private static ServerSocket serverSocket;

    /**
     * Constructeur par defaut
     */
    public Commande() {
        super();
    }

    /**
     * Commande mybrowsersync
     * 
     * @param file de l'emplacement en cours
     * @param source commande lu
     */
    public static void mybrowsersync(final File file, final String source) {
        
        try {
            serverSocket = new ServerSocket(ServeurHTTPJava.PORT);
            System.out.println("Serveur activée.\nAttente de connexion client au port : " + ServeurHTTPJava.PORT + " ...\n");

            // Attente de la connection client
            while (true) {
                ServeurHTTPJava myServer = new ServeurHTTPJava(serverSocket.accept());

                if (ServeurHTTPJava.verbose) {
                    System.out.println("Connection ok !");
                }

                // thread de gestion de la connection
                Thread threadClient = new Thread(myServer);
                threadClient.start();
            }

        } catch (final IOException e) {
            e.printStackTrace();
        } finally {
            try {
                serverSocket.close();
            } catch (final IOException e) {
                e.printStackTrace();
            }
        }

    }

    /**
     * @return file de l'emplacement
     */
    public File getFile() {
        return file;
    }

    /**
     * @param file de l'emplacement
     */
    public void setFile(final File file) {
        this.file = file;
    }

}
