/**
 * 
 */
package validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Permet de valider les commandes
 * 
 * @author Charline
 *
 */
public class CommandeValidator {

    /**
     * Constructeur par defaut
     */
    public CommandeValidator() {
        super();
    }
       
    /**
     * permet de valider la commande mybrowsersync
     * 
     * @param cmd lu
     * @return true si le format est respect� sinon false
     */
    public static boolean isValideMybrowsersync(final String cmd) {
        final String regExpPwd = "^mybrowsersync$";
        final Pattern pattern = Pattern.compile(regExpPwd);
        final Matcher matcher = pattern.matcher(cmd);
        if (matcher.matches()) {
            return true;
        }
        return false;
    }
}
