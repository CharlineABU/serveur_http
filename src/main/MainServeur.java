/**
 * 
 */
package main;

import java.io.File;

import action.Commande;
import action.Gestion;
import util.ScannerUtil;

/**
 * @author Charline
 *
 */
public class MainServeur {

    /**
     * @param args tableau d'argument(s)
     */
    public static void main(final String[] args) {
        
        ScannerUtil.ouvrir();
        
        final Commande commandes = new Commande();
        final File file = new File(commandes.getFile().getPath());

        System.out.print(file.getAbsolutePath() + ">");
        String cmd = ScannerUtil.lire();
        Gestion.gestionCommande(file, cmd);

        ScannerUtil.fermer();

    }

}
