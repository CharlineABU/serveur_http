/**
 * Gestion du projet
 */
package util;

import java.util.Scanner;

/**
 * Classe ScannerUtil : permet de gerer la lecture des informations <br>
 * saisis par l'utilisateur dans tous le projet
 * 
 * @author Charline
 *
 */
public class ScannerUtil {

    public static Scanner scan;

    /**
     * Ouverture du scanner
     */
    public static void ouvrir() {
        scan = new Scanner(System.in);
    }

    /**
     * Lecture du scanner
     * 
     * @return la Chaine de caractère lue
     */
    public static String lire() {
        return scan.nextLine();
    }

    /**
     * Fermeture du scanner
     */
    public static void fermer() {
        scan.close();
    }

}
